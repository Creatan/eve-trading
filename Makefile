.PHONY: run, update-db, setup, update-market-db

run:
	python run.py run

update-db:
	@echo "Fetching dump..."
	wget https://www.fuzzwork.co.uk/dump/postgres-latest.dmp.bz2
	@bunzip2 postgres-latest.dmp.bz2
	@echo "Restoring database..."
	@sudo -u postgres createdb "eve-galatea" --encoding UTF-8 --lc-collate en_US.utf8 --lc-ctype en_US.utf8 --template template0
	@sudo -u postgres pg_restore  -d "eve-galatea" postgres-latest.dmp
	@echo "Cleaning..."
	@rm postgres-latest.dmp
	@echo "Done!"

update-market-db:
	@echo "from app import db; db.create_all(bind='evemarket')" | python

setup:
	sudo -u postgres createuser -P -S -D -R evemarket
	sudo -u postgres createdb "evemarket" --encoding UTF-8 --lc-collate en_US.utf8 --lc-ctype en_US.utf8 --template template0
	