from evemanager import db

"""
SQLAlchemy mappings for the Eve static data export
"""
class invTypes(db.Model):
    __table__ = db.Model.metadata.tables['invTypes']

    def __repr__(self):
        return '<invTypes %r>' % self.typeName


class invMetaTypes(db.Model):
    __table__ = db.Model.metadata.tables['invMetaTypes']

    def __repr__(self):
        return '<invMetaTypes %r>' % self.invMetaTypeID


class invGroups(db.Model):
    __table__ = db.Model.metadata.tables['invGroups']

    def __repr__(self):
        return '<invGroups %r>' % self.groupName


class invCategories(db.Model):
    __table__ = db.Model.metadata.tables['invCategories']

    def __repr__(self):
        return '<invCategories %r>' % self.categoryName

class mapRegions(db.Model):
    __table__ = db.Model.metadata.tables['mapRegions']

    def __repr__(self):
        return '<mapRegions {}>'.format(self.regionID)

class mapSolarSystems(db.Model):
    __table__ = db.Model.metadata.tables['mapSolarSystems']

    def __repr__(self):
        return '<mapSolarSystems {}>'.format(self.solarSystemID)

class industryActivityMaterials(db.Model):
    __table__ = db.Model.metadata.tables['industryActivityMaterials']
    __mapper_args__ = {
        'primary_key':[db.Model.metadata.tables['industryActivityMaterials'].c.typeID, db.Model.metadata.tables['industryActivityMaterials'].c.materialTypeID]
    }
    def __repr__(self):
        return '<industryActivityMaterials %r>' % "Activotu"

class industryActivityProducts(db.Model):
    __table__ = db.Model.metadata.tables['industryActivityProducts']
    __mapper_args__ = {
        'primary_key':[db.Model.metadata.tables['industryActivityProducts'].c.typeID, db.Model.metadata.tables['industryActivityProducts'].c.productTypeID]
    }
    def __repr__(self):
        return '<industryActivityProducts %r>' % self.typeID 

class dgmTypeAttributes(db.Model):
    __table__ = db.Model.metadata.tables['dgmTypeAttributes']

    def __repr__(self):
        return '<objectDetails {}>'.format(self.attributeID)