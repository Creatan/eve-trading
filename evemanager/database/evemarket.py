from flask.ext.login import UserMixin
from sqlalchemy.ext.hybrid import hybrid_property
from evemanager import db, bcrypt

class Incursions(db.Model):
    __bind_key__ = 'evemarket'
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date())
    duration = db.Column(db.String(120))
    payment = db.Column(db.Integer)
    sites = db.Column(db.Integer)

    def __init__(self, date, duration, payment, sites):
        self.date = date
        self.duration = duration
        self.payment = payment
        self.sites = sites

    def __repr__(self):
        return '<Incursions %r>' % self.id

class APIKeys(db.Model):
    __bind_key__ = 'evemarket'
    id = db.Column(db.Integer, primary_key=True)
    keyid = db.Column(db.String(20))
    vcode = db.Column(db.Text())

    def __init__(self,keyid,vcode):
        self.keyid = keyid
        self.vcode = vcode

    def __repr__(self):
        return '<APIKey {}>'.format(self.keyid)

class Characters(db.Model):
    __bind_key__ = 'evemarket'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250))
    characterID = db.Column(db.Integer)
    corporationID = db.Column(db.Integer)
    corporationName = db.Column(db.Text)
    allianceId = db.Column(db.Integer)
    allianceName = db.Column(db.Text)
    factionID = db.Column(db.Integer)
    factionName = db.Column(db.Text)
    selected = db.Column(db.Boolean,default=False)
    apiID = db.Column(db.Integer,db.ForeignKey(APIKeys.id))
    api = db.relationship('APIKeys',backref=db.backref('characters',lazy='dynamic'))


    def __init__(self,name,characterID,corporationID,corporationName,allianceId,allianceName,
                 factionID,factionName,apiID):
        self.name = name
        self.characterID = characterID
        self.corporationID = corporationID
        self.corporationName = corporationName
        self.allianceId = allianceId
        self.allianceName = allianceName
        self.factionID = factionID
        self.factionName = factionName
        self.apiID = apiID

    def __repr__(self):
        return '<Character {}>'.format(self.name)

class Users(UserMixin,db.Model):
    __bind_key__ = 'evemarket'
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(64), unique=True)
    _password = db.Column(db.String(128))
    
    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def _set_password(self, plaintext):
        self._password = bcrypt.generate_password_hash(plaintext)

    def is_correct_password(self, plaintext):
        auth = bcrypt.check_password_hash(self._password, plaintext)
        return auth
