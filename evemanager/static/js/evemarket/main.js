function updateTip(){
	var sites = $('input[name="sites"').val();
	var tipPerSite = 1000000;
	$('.tip').html(_.formatNumber(parseInt(sites,10)*tipPerSite));
}

function updateSiteRan(){
	$('.last-site').html(_.quicktime());
}
$(document).ready(function(){
	//maincontent to fill viewport
	$header = $('body > .navbar')
	$('.main').css('minHeight',window.innerHeight - $header.outerHeight());
	
	//for some reason some pages were scrolled bottom so we get it back up
	window.scrollTo(0,0);

	$('.site-decrease').on('click',function(){
		var $input = $('input[name="sites"');
		var sites = parseInt($input.val(),10) == 0 ? 1 : parseInt($input.val(),10);
		$input.val(sites-1);
		updateTip();
	});
	$('.site-increase').on('click',function(){
		var $input = $('input[name="sites"');
		$input.val(parseInt($input.val(),10)+1);
		updateTip();
		updateSiteRan();
	});
	// format the starting and ending isk amount
	$("#incursions").on('change','.money',function(){
		$(this).val(_.formatNumber($(this).val()));
	});

	// doubleclick to input current time in utc
	$("#incursions").on('dblclick','.time',function(){
		$(this).val(_.quicktime());
	});

	// Modal handling for easier paying of tips to boosters
	// We create a modal window that has text input that will be populated by the
	// tip amount without formatting and it will be preselected
	$('#tipModal').on('focus','input[type="text"]',function(){
		this.select();
	})

	// populate input
	$('#tipModal').on('show.bs.modal', function (event) {
		var modal = $(this),
  		tipInput = modal.find('.modal-body input');
  		tipInput.val(_.unformat($('.tip').text()));
	});
	// set focus to the input
	$('#tipModal').on('shown.bs.modal',function(){
		$('#tipPayment').focus();
	});

	//submenu behaviour
	$('.sub-menu-toggle').on('click',function(e){
		e.preventDefault();
		var $icon = $(this).find('span');
		if($icon.hasClass('fa-angle-left')){
			$icon.removeClass('fa-angle-left').addClass('fa-angle-down');
		}
		else{
			$icon.removeClass('fa-angle-down').addClass('fa-angle-left');
		}
		var $list_item = $(this).parent('li');
		$list_item.find('.sub-menu').slideToggle(300);
	});

	//Set default character from character listing
	$('.characters').on('click','.character-default',function(){
		var src = $(this).parents('tr').find('img').attr('src').split('/'),
		id = src[4].split('_'); 
		id = id[0];
		$.ajax({
			url:'/settings/characters/default',
			method:'POST',
			data:{characterid:id},
			success:function(data){
				window.location.href='/settings/characters';
			}
		})
	});

	//fetch account balance automaticly
	$('.fetch-isk').on('click',function(){
		$.ajax({
			url:'/accountbalance',
			success:function(data){
				//convert balance to string
				var balance = data.result.balance+'';
				//split off cents, we don't need them
				balance = balance.split('.')[0];
				
				$('input[name="start-isk"]').val(_.formatNumber(balance));
			}
		})
	});
});