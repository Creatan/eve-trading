var _ = (function(){
	return {

		pad: function (number){
			// slice -2 returns 2 last characters so
			// we get the number as zero padded even if it was already padded
			return ('0'+number).slice(-2);
		},

		/*
		Format the number with thousand separators
		*/
		formatNumber: function (number, separator){
			if(typeof separator == 'undefined'){
				separator = ','
			}
			var base = parseInt(Math.abs(this.unformat(number) || 0),10) + "",
			mod = base.length > 3 ? base.length % 3 : 0;
			return (mod ? base.substr(0, mod) + separator : "") + base.substr(mod).replace(/(\d{3})(?=\d)/g, "$1" + separator);
		},

		unformat: function (number){
			//if it's number return it straight
			if(typeof number == 'number') return number;
			return number.replace(/[^0-9]/g,'');
		},
		quicktime:function quickTime(){
			var time = new Date()
			//utc-time because of eve
			return time.getUTCHours()+":"+this.pad(time.getUTCMinutes());
		},
	};
})();