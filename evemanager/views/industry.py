#needed for / sign to return float, otherwise the result will be cast into int and returns zero...
from __future__ import division

from flask import Flask, flash, render_template, request, redirect, url_for, session
from flask.ext.login import login_required
import datetime as dt
import math

from evemanager import app, db
from evemanager.utils import api, get_auth
from evemanager.database.static import invTypes, invCategories, invMetaTypes, invGroups
from evemanager.database.static import industryActivityMaterials, industryActivityProducts




@app.route('/industry/jobs')
@login_required
def jobs():
	# @TODO: Get activies from db / api
    activities = {
        1: 'Manufacturing',
        2: 'Techonology',
        3: 'Time Efficiency',
        4: 'Material Efficiency',
        5: 'Copying',
        6: 'Duplicating',
        7: 'Reverse Engineering',
        8: 'Invetion'
    }
    rowClasses = {
        1: 'warning',
        2: '',
        3: '',
        4: '',
        5: 'danger',
        6: '',
        7: '',
        8: 'info'
    }
    auth = get_auth()
    characters = auth.account.Characters()
    jobs = {}
    for character in characters.characters:
        industryJobs = auth.char.IndustryJobs(characterID=character.characterID)
        if industryJobs.jobs:
            jobs[character.name] = []

            for job in industryJobs.jobs:
                rowClass = ''
                ttc = dt.datetime.fromtimestamp(job.endDate)-dt.datetime.utcnow()
                if ttc.total_seconds() < 0:
                    ttc = 'Done'
                    rowClass = 'success'
                else:
                    if ttc.days > 0:
                        hours,remainder = divmod(ttc.seconds,3600)
                        minutes,seconds = divmod(remainder,60)
                        ttc = '{:d}D {:02d}H {:02d}M'.format(ttc.days, hours, minutes)
                    else:
                        hours,remainder = divmod(ttc.seconds,3600)
                        minutes,seconds = divmod(remainder,60)
                        ttc = '{:02d}H {:02d}M'.format(hours, minutes)

                if rowClass == '':
                    rowClass = rowClasses[job.activityID]
                formatedJob = {
                    'blueprintTypeName' : job.blueprintTypeName,
                    'activityID' : activities[job.activityID],
                    'status' : job.status,
                    'ttc': ttc,
                    'rowClass':rowClass
                }

                jobs[character.name].append(formatedJob)
        
    colSize = 12
    if(len(jobs) == 0):
        colSize = 12
    else:
        colSize = colSize / len(jobs)

    return render_template("industry/jobs.html", jobs=jobs, colSize=colSize)


@app.route('/industry/kitbuilder', methods=['POST','GET'])
@login_required
def kitbuilder():
    runs=10
    items = db.session.query(invTypes.typeID, invTypes.typeName).\
        join(invMetaTypes, invMetaTypes.typeID == invTypes.typeID).\
        join(invGroups, invGroups.groupID == invTypes.groupID).\
        join(invCategories, invCategories.categoryID == invGroups.categoryID).\
        filter(invCategories.categoryID == 7,
               invMetaTypes.metaGroupID == 2, invTypes.marketGroupID != None,
               ~invTypes.groupID.in_([773, 774, 775, 776, 777, 778, 779, 781, 
                782, 786, 787,1233, 1223])
        ).order_by(invTypes.typeName.asc()).all()
    materials = []

    if(request.method == 'POST'):
        #find materials
        materials = db.session.query(industryActivityMaterials, invTypes.typeID, invTypes.typeName,
            industryActivityMaterials.quantity).\
        join(invTypes, invTypes.typeID == industryActivityMaterials.materialTypeID).\
        join(industryActivityProducts, industryActivityMaterials.typeID == industryActivityProducts.typeID).\
        filter(industryActivityProducts.productTypeID == request.form['blueprint'],
            industryActivityMaterials.activityID == 1,
            ).order_by(invTypes.typeName.asc()).all()

        for material in materials:
            quantities = round(runs * material.quantity * ((100-int(request.form['ME']))/100))
            material.quantity = int(max(runs, math.ceil(quantities))) * int(request.form.get('kits'))
    return render_template("industry/kitbuilder.html",items=items, materials=materials)