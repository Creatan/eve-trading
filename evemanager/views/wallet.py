from flask import Flask, flash, render_template, request, redirect, url_for, session,jsonify
from flask.ext.login import login_required
import datetime as dt
from evemanager import app
from evemanager.utils import api, get_auth

@app.route('/wallet/journal')
@login_required
def wallet():
    refTypes =  {}
    for type in api.eve.RefTypes().refTypes:
        refTypes[type['refTypeID']] = type['refTypeName']

    
    entries = []
    # Maximum for 64bit int start from there and find the lowest id in rows
    lowestID = 9223372036854775807
    auth = get_auth()
    journalData = auth.char.WalletJournal(characterID = session.get('character').get('id'),rowCount=2000).transactions  

    # Walk backwards in the wallet journal until we don't get any more results
    while(len(journalData) != 0):
        for row in journalData:
            if float(row['amount']) < 0:
                css_class = 'danger'
            else:
                css_class = 'success'

            entries.append({
                'date':dt.datetime.fromtimestamp(row['date']),
                'type':refTypes.get(row['refTypeID'],'Unknown'),
                'amount':'{0:,.2f}'.format(float(row['amount'])),
                'balance':'{0:,.2f}'.format(float(row['balance'])),
                'class': css_class
                })
            lowestID = min(lowestID,int(row['refID']))
        journalData = auth.char.WalletJournal(characterID = session.get('character').get('id'),rowCount=2000,fromID=lowestID).transactions  

    
    return render_template("wallet/journal.html",entries=entries)

@app.route('/wallet/transactions')
@login_required
def transactions():
    entries = []
    # Maximum for 64bit int start from there and find the lowest id in rows
    lowestID = 9223372036854775807
    auth = get_auth()
    transactions = auth.char.WalletTransactions(characterID = session.get('character').get('id'),rowCount=2000).transactions

    # Walk backwards in the wallet journal until we don't get any more results
    while(len(transactions) != 0):
        for row in transactions:
            if row['transactionType'] == 'buy':
                css_class = 'danger'
            else:
                css_class = 'success'
            
            entries.append({
                'date': dt.datetime.fromtimestamp(row['transactionDateTime']),
                'name':row['typeName'],
                'quantity':row['quantity'],
                'price':'{0:,.2f}'.format(float(row['price'])),
                'class': css_class
                })
            lowestID = min(lowestID,int(row['transactionID']))
        transactions = auth.char.WalletTransactions(characterID = session.get('character').get('id'),rowCount=2000,fromID=lowestID).transactions
    
    return render_template("wallet/transactions.html",entries=entries)



"""
Gives characters account balance as json
"""
@app.route('/accountbalance')
@login_required
def accountbalance():
    if 'character' in session:
        auth = get_auth()
        character = session.get('character')
        balance = auth.char.AccountBalance(characterID=character.get('id'))
         
    return jsonify(result={'balance':balance.accounts[0].balance})