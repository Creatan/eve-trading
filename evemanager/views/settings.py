from flask import Flask, flash, render_template, request, redirect, url_for
from flask.ext.login import login_required
from evemanager.utils import cache_search, save_cache
from evemanager import app, db
from evemanager.utils import api

from evemanager.database.static import mapRegions, mapSolarSystems
from evemanager.database.evemarket import APIKeys, Characters

@app.route('/settings')
@login_required
def settings():
    sorted_systems = cache_search('system-list')
    if sorted_systems == None:
        #TODO: simplify this. Join the queries

        # exclude wormholes from regions because there aren't any markets
        wormholeRegions = ['A-R00001','A-R00002','A-R00003',
                           'B-R00004','B-R00005','B-R00006',
                           'B-R00007','B-R00008','C-R00009',
                           'C-R00010','C-R00011','C-R00012',
                           'C-R00013','C-R00014','C-R00015',
                           'D-R00016','D-R00017','D-R00018',
                           'D-R00019','D-R00020','D-R00021',
                           'D-R00022','D-R00023','E-R00024',
                           'E-R00025','E-R00026','E-R00027',
                           'E-R00028','E-R00029','F-R00030']

        regions = db.session.query(mapRegions, mapRegions.regionID,mapRegions.regionName).\
                  filter(~mapRegions.regionName.in_(wormholeRegions)).all()
        
        #get the region ids of regions that doesn't include wormholes to 
        #be used with the solarsystem search exclusion
        regionIDs = []
        for region in regions:
            regionIDs.append(region.regionID)

        systems = db.session.query(mapSolarSystems, mapSolarSystems.solarSystemName,mapSolarSystems.solarSystemID,
                  mapSolarSystems.regionID).filter(mapSolarSystems.regionID.in_(regionIDs)).all()

        # group the systems by region
        sorted_systems = []
        #for region in regions:
        #    sorted_systems[region.regionID] = []

        for system in systems:
            if system.regionID in regionIDs:
                tmpsystem = {
                    'regionID': system.regionID,
                    'solarSystemName':system.solarSystemName,
                    'solarSystemID': system.solarSystemID
                }
            
                sorted_systems.append(tmpsystem)
        save_cache('system-list',sorted_systems)


    return render_template('settings.html',systems=sorted_systems)

@app.route('/settings/api',methods=['POST','GET'])
@login_required
def settings_api():
    
    keyinfo = db.session.query(APIKeys).all()
    apikeys = []
    for key in keyinfo:
        apikey = {
            'keyid':key.keyid,
            'vcode':key.vcode,
            'characters':[]
        }
        for characters in key.characters.order_by(Characters.characterID.desc()).all():
            apikey['characters'].append(characters.characterID)
        apikeys.append(apikey)
    return render_template('settings/api.html',apikeys=apikeys)

@app.route('/settings/api/add',methods=['POST'])
def settings_api_add():
    try:
        auth = api.auth(keyID=request.form['keyid'],vCode=request.form['vcode'])
        characters = auth.account.Characters()
        
        apikey = APIKeys(request.form['keyid'],request.form['vcode'])
        db.session.add(apikey)
        db.session.commit()
        
        for character in characters.characters:
            character = Characters(character.name,character.characterID,character.corporationID,
                        character.corporationName,character.allianceID,character.allianceName,
                        character.factionID,character.factionName,apikey.id)
            db.session.add(character)
            db.session.commit()

    except eveapi.Error, e:
        print e.code
        print e.message
    except RuntimeError, e:
        flash('Invalid key id / verification code')

    return redirect(url_for('settings_api'))

@app.route('/settings/characters',methods=['GET'])
@login_required
def characters():
    characters = Characters.query.order_by(Characters.characterID).all()
    return render_template('settings/characters.html',characters=characters)

@app.route('/settings/characters/default',methods=['POST'])
@login_required
def characters_default():
    characters = Characters.query.all()
    print request.form.get('characterid')
    for character in characters:
        print character.characterID
        if character.characterID == int(request.form.get('characterid')):
            print 'true'
            character.selected = True
        else:
            character.selected = False
        db.session.add(character)
    db.session.commit()
    return ''



    
@app.route('/settings/characters/activate',methods=['GET'])
@login_required
def characters_activate():
    pass
