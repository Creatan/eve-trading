from flask import Flask, flash, render_template, request, redirect, url_for, session
from flask.ext.login import login_required
from operator import itemgetter
import xml.etree.ElementTree as ET
import json

from evemanager import app,db
from evemanager.utils import request_parts, cache_request, cache_search, save_cache,update_cache_expiry
from evemanager.database.static import invTypes, invCategories, invMetaTypes, invGroups


@app.route('/market/modules')
@login_required
def market():
    """
    @TODO: Move updating cache to backround workers that are invoked from the page
    so that we don't timeout for the page display.
    """
    evecentral = 'http://api.eve-central.com/api/marketstat?typeid='
    items = db.session.query(invTypes.typeID, invTypes.typeName).\
        join(invMetaTypes, invMetaTypes.typeID == invTypes.typeID).\
        join(invGroups, invGroups.groupID == invTypes.groupID).\
        join(invCategories, invCategories.categoryID == invGroups.categoryID).\
        filter(invCategories.categoryID == 7,
               invMetaTypes.metaGroupID == 2, invTypes.marketGroupID != None)

    
    idlist = []  
    for item in items.all():
        idlist.append(str(item[0]))
    """
    cut the id list to smaller pieces because otherwise
    it will go over the url char limit
    """
    parts = list(request_parts(idlist,100))

    modules = {}
    for part in parts:
        
        ids = '&typeid='.join(part)
        jita_url = '{0}{1}&usesystem=30000142'.format(evecentral,ids)
        jita = cache_request(jita_url,3600)
        buyprices = ET.fromstring(jita)

        dodixie_url = jita_url
        #dodixie_url = '{0}{1}&usesystem=30002659'.format(evecentral,ids)
        #dodixie_url = '{0}{1}&usesystem=30001376'.format(evecentral,ids)
        dodixie = cache_request(dodixie_url,3600)
        sellprices = ET.fromstring(dodixie)

        for typeID in part:
            module = {}
            sellstring = "marketstat/type[@id='{0}']".format(typeID)
            sellElement = sellprices.find(sellstring)
            prices = sellElement.find('sell')
            sellMin = float(prices.find('min').text)
            module['sell'] = sellMin 
            
            buystring = "marketstat/type[@id='{0}']".format(typeID)
            buyelement = buyprices.find(buystring)
            buy = buyelement.find('buy')
            buyMax = float(buy.find('max').text)
            module['buy'] = buyMax

            module['gross'] = sellMin - buyMax
            module['fees'] = sellMin*((0.0075)+(0.009))+buyMax*(0.0075) 
            module['net'] = module['gross'] - module['fees']
            if sellMin > 0:
                module['marginpercent'] = (module['net'] / sellMin)*100
            else:
                module['marginpercent'] = 0

            if module['marginpercent'] > 15:
                module['class'] = 'success'
            else:
                module['class'] = ''
                
            history_url = 'https://public-crest.eveonline.com/market/10000002/types/{0}/history/'.format(typeID)
            #history_url = 'https://public-crest.eveonline.com/market/10000033/types/{0}/history/'.format(typeID)
            history = cache_request(history_url,3600*24)
            try:
                history_data = json.loads(history)
                history_items = history_data['items']
                history_slice = history_items[-15:]
                volume = 0
                for item in history_slice:
                    volume = volume + item['volume']
                if len(history_slice) > 0:
                    module['volume'] = int(volume / len(history_slice))
                else:
                    module['volume'] = 'N/A'

                if module['volume'] != 'N/A':
                    module['potential_day'] = module['net'] * module['volume']
                    module['volume_month'] = module['volume']*30
                    module['potential_month'] = '{0:,.2f}'.format(module['potential_day']*30)
                    module['potential_day'] = '{0:,.2f}'.format(module['potential_day'])
                else:
                    module['potential_day'] = 'N/A'
                    module['volume_month'] = 'N/A'
                    module['potential_month'] = 'N/A'
            except TypeError:
                print history
                module['volume'] = 'N/A'                
                module['potential_day'] = 'N/A'
                module['volume_month'] = 'N/A'
                module['potential_month'] = 'N/A'
            modules[typeID] = module
    
    results = []
    try:
        for item in items:
            if modules[str(item.typeID)]['net'] > 0 and modules[str(item.typeID)]['volume'] != "N/A":
                results.append({'typeID':item.typeID,'typeName':item.typeName,'sell':'{0:,.2f}'.format(modules[str(item.typeID)]['sell']),
                    'buy':'{0:,.2f}'.format(modules[str(item.typeID)]['buy']), 'fees':modules[str(item.typeID)]['fees'], 
                    'gross':modules[str(item.typeID)]['gross'],
                    'net':'{0:,.2f}'.format(modules[str(item.typeID)]['net']),
                    'marginpercent':'{0:,.2f}'.format(modules[str(item.typeID)]['marginpercent']),
                    'class':modules[str(item.typeID)]['class'],'volume':modules[str(item.typeID)]['volume'],
                    'potential_day':modules[str(item.typeID)]['potential_day'],
                    'volume_month':modules[str(item.typeID)]['volume_month'],
                    'potential_month':modules[str(item.typeID)]['potential_month']})        
    except:
        print item
        print modules
    results = sorted(results,key=itemgetter('volume'),reverse=True)

    return render_template("market/modules.html", items=results)
