from flask import Flask, flash, render_template, request, redirect, url_for
from flask.ext.login import login_user, logout_user, login_required
from evemanager import app
from evemanager.database.evemarket import Users
from evemanager.utils import get_redirect_target


@app.route('/signin', methods=['POST','GET'])
def signin():
	if request.method == 'POST':
		user = Users.query.filter_by(username=request.form.get('username')).first_or_404()
		if user.is_correct_password(request.form.get('password')):
			login_user(user)
			#TODO workout redirect to clean the url
			return redirect(get_redirect_target() or url_for('index'))

	return render_template('users/signin.html')


@app.route('/signout')
@login_required
def signout():
    logout_user()
    return redirect(url_for('index'))