from flask import session, request
import eveapi
import redis
import hashlib
import requests
import itertools
import cPickle
import datetime
import time

from urlparse import urlparse, urljoin

redis_server = redis.StrictRedis()

class RedisEveApiCacheHandler():
    def __init__(self, debug=False):
        self.debug = debug
        self.r = redis.StrictRedis()
  
    def log(self, what):
        if self.debug:
            print "[%s] %s" % (datetime.datetime.now().isoformat(), what)
  
    def retrieve(self, host, path, params):
        key = hash((host, path, frozenset(params.items())))
  
        cached = self.r.get(key)
        if cached is None:
            self.log("%s: not cached, fetching from server..." % path)
            return None
        else:
            cached = cPickle.loads(cached)
            if time.time() < cached[0]:
                self.log("%s: returning cached document" % path)
                return cached[1]
            self.log("%s: cache expired, purging !" % path)
            self.r.delete(key)
  
    def store(self, host, path, params, doc, obj):
        key = hash((host, path, frozenset(params.items())))
  
        cachedFor = obj.cachedUntil - obj.currentTime
        if cachedFor:
            self.log("%s: cached (%d seconds)" % (path, cachedFor))
  
            cachedUntil = time.time() + cachedFor
            cached = self.r.set(key, cPickle.dumps((cachedUntil, doc), -1))

api = eveapi.EVEAPIConnection(cacheHandler=RedisEveApiCacheHandler())

def cache_request(url, TTL=36):
    key = hashlib.sha224(url).hexdigest()

    if(redis_server.get(key)):
        print "request from redis"
        return redis_server.get(key)
    else:
        print "new request"
        r = requests.get(url)
        if r.status_code == 200:
            redis_server.set(key,r.text)
            redis_server.expire(key,TTL)
            return r.text
        else:
            return r.status_code
 
def cache_search(search):
    if(redis_server.get(search)):
        print "query data from cache"
        return redis_server.get(search)
    else:
        return None

def save_cache(search,result,TTL=3600):
    print "saved query data"
    redis_server.set(search,result)
    redis_server.expire(search,TTL)

def clear_cache(search):
    redis_server.delete(search)

def update_cache_expiry(url,TTL):
    key = hashlib.sha224(url).hexdigest()
    if(redis_server.get(key)):
        redis_server.expire(key,TTL)

def request_parts(iterable,size):
    it = iter(iterable)
    item = list(itertools.islice(it, size))
    while item:
        yield item
        item = list(itertools.islice(it, size))

def in_sub_page(active,pages):
    return any(active in page for page in pages)

def get_auth():
    character = session.get('character')
    auth = api.auth(keyID=character.get('keyid'),vCode=character.get('vcode'))
    return auth

def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

def get_redirect_target():
    for target in request.args.get('next'), request.referrer:
        if not target:
            continue
        if is_safe_url(target):
            return target
        else:
            return None
def set_character(character):
    api = character.api
    session['character'] = {
        'id':    character.characterID,
        'name':  character.name,
        'keyid': api.keyid,
        'vcode': api.vcode
    }
