from __future__ import division
from flask import Flask, flash, render_template, request, redirect, url_for, session, jsonify
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
from flask.ext.bcrypt import Bcrypt
from flask.ext.login import LoginManager, login_user, logout_user, login_required

import re
import datetime as dt
from utils import in_sub_page, get_auth, set_character
import gzip


#app setup
app = Flask(__name__)
app.config.from_object('config')
#custom test to determine if active page is a subpage
#used to set dropdowns open on page load
app.jinja_env.tests['insubpage'] = in_sub_page

db = SQLAlchemy(app)
#reflect tables from the eve database
db.Model.metadata.reflect(db.engine)


#setup for database migrations using Flask-Migrate
migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

#setup bcrypt for app
bcrypt = Bcrypt(app)

#import database classes. they depend from the app setups
from database.evemarket import Users,Incursions, Characters

#setup loginmanager
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view =  "signin"

@login_manager.user_loader
def load_user(userid):
    return Users.query.filter(Users.id==userid).first()


@app.before_request
def before_request():
    character = Characters.query.filter(Characters.selected==True).first()
    if 'character' not in session:
        if character is not None:
            set_character(character)       
    else:
        if character is not None and session.get('character') != character.characterID:
            set_character(character)

import  views
#TODO: Move routes to views.py
@app.route('/')
@login_required
def index():
    return render_template('index.html')


@app.route('/incursions', methods=['POST','GET'])
@app.route('/incursions/<int:page>', methods=['POST','GET'])
@login_required
def incursions(page=1):
    if(request.method == 'POST'):
        
        start_time = request.form['start-time'].split(':')
        end_time = request.form['end-time'].split(':')
        start = dt.datetime.combine(dt.date.today(),dt.time(int(start_time[0]),int(start_time[1])))
        end = dt.datetime.combine(dt.date.today(),dt.time(int(end_time[0]),int(end_time[1])))

        td = end - start
        duration = ':'.join(str(td).split(':')[:2])
        payout = int(re.sub(r'[^0-9]','',request.form['end-isk'])) - int(re.sub(r'[^0-9]','',request.form['start-isk']))
        
        incursion = Incursions(dt.date.today(),duration,payout,request.form['sites'])
        db.session.add(incursion)
        db.session.commit()

    incursions = Incursions.query.order_by(Incursions.id.desc()).paginate(page,10,False)
    
    return render_template('incursions.html',incursions=incursions)

@app.route('/plans')
@login_required
def plans():
    import os
    import xml.etree.ElementTree as ET
    from database.static import dgmTypeAttributes, invTypes

    skillpoints = {
        '1':250,
        '2':1415,
        '3':8000,
        '4':45255,
        '5':256000
    }

    #paths getting the correct path for the plan
    script_dir = os.path.dirname(__file__)
    relpath = 'plans/Dropbears Stage 0.emp'
    skills = {}

    #.emp is an gzip file
    archive = gzip.GzipFile(os.path.join(script_dir,relpath), 'r')
    #read a file inside the archive without extracting it to a tmp file
    xmldata = archive.read('Dropbears Stage 0')

    plan = ET.fromstring(xmldata)
    entries = plan.findall('entry')
    for entry in entries:
        skill = entry.attrib.get('skill')
        level = entry.attrib.get('level')
        if skill in skills:
            if level > skills.get(skill):
                skills[skill] = level
        else:
            skills.update({skill:level})
    skillplan = {}
    planskillpointstotal = 0
    skill_data = db.session.query(invTypes,invTypes.typeID, invTypes.typeName, dgmTypeAttributes.valueFloat).\
                 join(dgmTypeAttributes, dgmTypeAttributes.typeID == invTypes.typeID).\
                 filter(dgmTypeAttributes.attributeID == 275).all()
    for data in skill_data:
        if skills.get(data.typeName):
            sp = data.valueFloat*skillpoints.get(skills.get(data.typeName))
            planskillpointstotal = planskillpointstotal + sp
            skillplan.update({data.typeID:{'skillpoints':sp,'name':data.typeName}})

    auth = get_auth()
    charactersheet = auth.char.CharacterSheet(characterID=session.get('character').get('id'))
    skillpoints_trained = 0

    for skill in charactersheet.skills:
        if skill.typeID in skillplan.keys():
            required_sp = int(skillplan.get(skill.typeID).get('skillpoints'))
            if skill.skillpoints > required_sp or skill.skillpoints == required_sp:
                skillpoints_trained = skillpoints_trained + required_sp
            else:
                skillpoints_trained = skillpoints_trained + skill.skillpoints
    percent = (skillpoints_trained / planskillpointstotal) * 100
    return "Plan {0:.2f}% trained".format(percent)
